#ENVIRONMENT

## Requisitos
Para poder levantar todos los componentes es necesario crear previamente las imagenes del componente Web y del componente API. Para ello sigue las
instrucciones descritas en los repositorios:

* [API rest](https://bitbucket.org/dsanjuan-brujula/kerberos-api/src/master/)
* [Web](https://bitbucket.org/dsanjuan-brujula/kerberos/src/master/) 

## Deploy
Para levantar todos los servicios, situado en el directorio raiz donde se encuentra el fichero docker-compose, ejecutar:

```bash
docker-compose up
```